import feather from "feather-icons";

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('a[target="_blank"]').forEach(el => {
        if (!el.classList.contains('no-icon')) {
            el.innerHTML += `<i data-feather="external-link"></i>`;
        }
    });

    feather.replace();
});
