export function updateTooltips(): void {
    console.debug('Updating tooltips');
    const elements = document.querySelectorAll<HTMLElement>('.tip, .dropdown');

    // Calculate max potential displacement
    let max = 0;
    elements.forEach(el => {
        const box = el.getBoundingClientRect();
        if (max < box.height) max = box.height;
    });

    // Prevent displacement
    elements.forEach(el => {
        if (!el.dataset.tooltipSetup) {
            el.dataset.tooltipSetup = 'true';
            const box = el.getBoundingClientRect();
            if (box.bottom >= document.body.clientHeight - (max + 32)) {
                el.classList.add('top');
            }
        }
    });
}

document.addEventListener('DOMContentLoaded', () => {
    window.addEventListener('popstate', () => {
        updateTooltips();
    });
    window.requestAnimationFrame(() => {
        updateTooltips();
    });
});
