import feather from "feather-icons";

document.addEventListener('DOMContentLoaded', () => {
    const messageTypeToIcon: { [p: string]: string } = {
        info: 'info',
        success: 'check',
        warning: 'alert-triangle',
        error: 'x-circle',
        question: 'help-circle',
    };

    document.querySelectorAll<HTMLElement>('.message').forEach(el => {
        const icon = el.querySelector('.icon');
        const type = el.dataset['type'];
        if (!icon || !type) return;
        if (!messageTypeToIcon[type]) throw new Error(`No icon for type ${type}`);

        const svgContainer = document.createElement('div');
        svgContainer.innerHTML = feather.icons[messageTypeToIcon[type]].toSvg();

        if (svgContainer.firstChild) el.insertBefore(svgContainer.firstChild, icon);
        icon.remove();
    });

    feather.replace();
});
