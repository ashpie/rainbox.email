document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.copyable-text').forEach(el => {
        const contentEl = el.querySelector('.content');
        const selection = window.getSelection();
        if (contentEl && selection) {
            contentEl.addEventListener('click', () => {
                selection.selectAllChildren(contentEl);
            });
            el.querySelector('.copy-button')?.addEventListener('click', () => {
                selection.selectAllChildren(contentEl);
                document.execCommand('copy');
            });
        }
    });
});
