document.addEventListener('DOMContentLoaded', () => {
    const menuButton = document.getElementById('menu-button');
    const mainMenu = document.getElementById('main-menu');

    if (menuButton) {
        menuButton.addEventListener('click', (e) => {
            e.stopPropagation();
            mainMenu?.classList.toggle('open');
        });
    }

    if (mainMenu) {
        mainMenu.addEventListener('click', (e) => {
            e.stopPropagation();
        });

        document.addEventListener('click', () => {
            mainMenu.classList.remove('open');
        });
    }
});
