document.addEventListener('DOMContentLoaded', () => {
    const createEmailAddress = document.querySelector<HTMLInputElement>('#field-create_email');
    const username = document.querySelector<HTMLInputElement>('#field-username');
    const email_username = document.querySelector<HTMLElement>('#email_username');
    const domain = document.querySelector<HTMLInputElement>('#field-domain');
    const recovery_email = document.querySelector('#field-recovery_email');
    const recovery_email_label = recovery_email?.parentElement?.querySelector<HTMLElement>('.hint');

    if (
        !createEmailAddress ||
        !recovery_email ||
        !recovery_email_label ||
        !domain ||
        !username ||
        !email_username
    ) {
        console.warn('Some elements wern\'t found.');
        return;
    }

    const updateForm = () => {

        if (createEmailAddress.checked) {
            recovery_email.removeAttribute('required');
            recovery_email_label.style.display = 'block';
            domain.disabled = false;
        } else {
            recovery_email.setAttribute('required', 'required');
            recovery_email_label.style.display = 'none';
            domain.disabled = true;
        }

        username.value = username.value.toLowerCase();
        email_username.innerText = username.value + '@';
    };

    createEmailAddress.addEventListener('change', updateForm);
    username.addEventListener('change', updateForm);
    username.addEventListener('keyup', updateForm);
    updateForm();
});
