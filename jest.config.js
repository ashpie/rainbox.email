module.exports = {
    transform: {
        "^.+\\.ts$": "ts-jest"
    },
    moduleFileExtensions: [
        'js',
        'ts',
        'd.ts'
    ],
    testMatch: [
        '**/test/**/*.test.ts'
    ],
    testEnvironment: 'node'
};