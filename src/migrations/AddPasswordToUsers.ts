import Migration from "wms-core/db/Migration";
import {Connection} from "mysql";
import ModelFactory from "wms-core/db/ModelFactory";
import User from "wms-core/auth/models/User";
import UserPasswordComponent from "../models/UserPasswordComponent";

export default class AddPasswordToUsers extends Migration {
    public async install(connection: Connection): Promise<void> {
        await this.query(`ALTER TABLE users
            ADD COLUMN password VARCHAR(128) NOT NULL`, connection);
    }

    public async rollback(connection: Connection): Promise<void> {
        await this.query(`ALTER TABLE users
            DROP COLUMN password`, connection);
    }

    public registerModels(): void {
        ModelFactory.get(User).addComponent(UserPasswordComponent);
    }
}
