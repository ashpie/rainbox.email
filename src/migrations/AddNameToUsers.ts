import Migration from "wms-core/db/Migration";
import ModelFactory from "wms-core/db/ModelFactory";
import User from "wms-core/auth/models/User";
import UserNameComponent from "../models/UserNameComponent";
import {Connection} from "mysql";

export default class AddNameToUsers extends Migration {
    public async install(connection: Connection): Promise<void> {
        await this.query(`ALTER TABLE users
            ADD COLUMN name VARCHAR(64) UNIQUE NOT NULL`, connection);
    }

    public async rollback(connection: Connection): Promise<void> {
        await this.query('ALTER TABLE users DROP COLUMN name', connection);
    }

    public registerModels(): void {
        ModelFactory.get(User).addComponent(UserNameComponent);
    }
}
