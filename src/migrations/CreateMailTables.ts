import Migration from "wms-core/db/Migration";
import {Connection} from "mysql";
import ModelFactory from "wms-core/db/ModelFactory";
import MailDomain from "../models/MailDomain";
import MailIdentity from "../models/MailIdentity";
import User from "wms-core/auth/models/User";
import UserMailIdentityComponent from "../models/UserMailIdentityComponent";

export default class CreateMailTables extends Migration {
    public async install(connection: Connection): Promise<void> {
        await this.query(`CREATE TABLE mail_domains
                          (
                              id      INT                 NOT NULL AUTO_INCREMENT,
                              name    VARCHAR(252) UNIQUE NOT NULL,
                              user_id INT,
                              PRIMARY KEY (id),
                              FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE SET NULL
                          )`, connection);
        await this.query(`CREATE TABLE mail_identities
                          (
                              id             INT         NOT NULL AUTO_INCREMENT,
                              user_id        INT         NOT NULL,
                              mail_domain_id INT         NOT NULL,
                              name           VARCHAR(64) NOT NULL,
                              redirects_to   VARCHAR(254),
                              PRIMARY KEY (id),
                              UNIQUE (mail_domain_id, name),
                              FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
                              FOREIGN KEY (mail_domain_id) REFERENCES mail_domains (id) ON DELETE CASCADE
                          )`, connection);
        await this.query(`ALTER TABLE users
            ADD COLUMN main_mail_identity_id INT,
            ADD FOREIGN KEY main_mail_identity_fk (main_mail_identity_id) REFERENCES mail_identities (id)`, connection);
    }

    public async rollback(connection: Connection): Promise<void> {
        await this.query(`ALTER TABLE users
            DROP FOREIGN KEY main_mail_identity_fk,
            DROP COLUMN main_mail_identity_id`, connection);
        await this.query(`DROP TABLE IF EXISTS mail_identities, mail_domains`, connection);
    }

    public registerModels(): void {
        ModelFactory.register(MailDomain);
        ModelFactory.register(MailIdentity);
        ModelFactory.get(User).addComponent(UserMailIdentityComponent);
    }

}
