import {MailTemplate} from "wms-core/Mail";

export const ADD_RECOVERY_EMAIL_MAIL_TEMPLATE: MailTemplate = new MailTemplate(
    'add_recovery_email',
    (data) => 'Add ' + data.email + ' as you recovery email.',
);
