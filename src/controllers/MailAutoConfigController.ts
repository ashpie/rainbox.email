import Controller from "wms-core/Controller";
import {Request, Response} from "express";
import MailDomain from "../models/MailDomain";
import config from "config";

export default class MailAutoConfigController extends Controller {
    public routes(): void {
        this.get([
            '/.well-known/autoconfig/mail/config-v1.1.xml',
            '/mail/config-v1.1.xml',
        ], this.getAutoConfig, 'mail-auto-config');
    }

    /**
     * See view and https://wiki.mozilla.org/Thunderbird:Autoconfiguration:ConfigFileFormat
     */
    protected async getAutoConfig(req: Request, res: Response): Promise<void> {
        res.contentType('text/xml');
        const domains = await MailDomain.select().get();
        res.render('mail-auto-config.xml.njk', {
            host_domain: config.get<string>('mail_autoconfig.main_domain'),
            domains: domains.sort((a, b) => a.name && b.name && a.name > b.name ? 1 : -1),
            display_name: config.get<string>('mail_autoconfig.display_name'),
            display_name_short: config.get<string>('mail_autoconfig.display_name_short'),
            username: config.get<string>('mail_autoconfig.username'),
            auth_method: config.get<string>('mail_autoconfig.auth_method'),
            imap: {
                server: config.get<string>('mail_autoconfig.imap.server'),
                port: config.get<string>('mail_autoconfig.imap.port'),
                method: config.get<string>('mail_autoconfig.imap.method'),
            },
            pop3: {
                server: config.get<string>('mail_autoconfig.pop3.server'),
                port: config.get<string>('mail_autoconfig.pop3.port'),
                method: config.get<string>('mail_autoconfig.pop3.method'),
            },
            smtp: {
                server: config.get<string>('mail_autoconfig.smtp.server'),
                port: config.get<string>('mail_autoconfig.smtp.port'),
                method: config.get<string>('mail_autoconfig.smtp.method'),
            },
        });
    }
}
