import Controller from "wms-core/Controller";
import BackendController from "wms-core/helpers/BackendController";
import {RequireAdminMiddleware, RequireAuthMiddleware} from "wms-core/auth/AuthComponent";
import {Request, Response} from "express";
import User from "wms-core/auth/models/User";
import {NotFoundHttpError} from "wms-core/HttpError";
import Validator from "wms-core/db/Validator";
import UserPasswordComponent from "../../models/UserPasswordComponent";
import UserNameComponent from "../../models/UserNameComponent";

export default class AccountBackendController extends Controller {

    public constructor() {
        super();

        BackendController.registerMenuElement({
            getLink: async () => Controller.route('backend-list-users'),
            getDisplayString: async () => 'Users',
            getDisplayIcon: async () => 'user',
        });
    }

    public getRoutesPrefix(): string {
        return '/backend/users';
    }

    public routes(): void {
        this.get('/', this.getListUsers, 'backend-list-users', RequireAuthMiddleware, RequireAdminMiddleware);
        this.get('/:user_id/change-password', this.getChangeUserPassword, 'backend-change-user-password', RequireAuthMiddleware, RequireAdminMiddleware);
        this.post('/:user_id/change-password', this.postChangeUserPassword, 'backend-change-user-password', RequireAuthMiddleware, RequireAdminMiddleware);
    }

    protected async getListUsers(req: Request, res: Response): Promise<void> {
        res.render('backend/users', {
            accounts: await User.select()
                .with('mainEmail')
                .get(),
        });
    }

    protected async getChangeUserPassword(req: Request, res: Response): Promise<void> {
        const user = await User.getById(req.params.user_id);
        if (!user) throw new NotFoundHttpError('user', req.url);

        res.render('backend/users-change-password', {
            user: user,
        });
    }

    protected async postChangeUserPassword(req: Request, res: Response): Promise<void> {
        const user = await User.getById(req.params.user_id);
        if (!user) throw new NotFoundHttpError('user', req.url);

        await this.validate({
            'new_password': new Validator().defined(),
            'new_password_confirmation': new Validator().sameAs('new_password', req.body.new_password),
        }, req.body);

        await user.as(UserPasswordComponent).setPassword(req.body.new_password, 'new_password');
        await user.save();

        req.flash('success', `New password set for ${user.as(UserNameComponent).name}`);
        res.redirect(Controller.route('backend-list-users'));
    }
}
