import Controller from "wms-core/Controller";
import {Request, Response} from "express";
import config from "config";

export default class HomeController extends Controller {
    public routes(): void {
        this.get('/', this.getHome, 'home');
        this.get('/about', this.getAbout, 'about');
        this.get('/back', this.goBack, 'back');
    }

    protected async getHome(req: Request, res: Response): Promise<void> {
        res.render('about', {
            instance_url: config.get('public_url'),
        });
    }

    protected async getAbout(req: Request, res: Response): Promise<void> {
        res.redirect(Controller.route('home'));
    }

    /**
     * This is to test and assert that wms-core extended types are available
     */
    protected async goBack(req: Request, res: Response): Promise<void> {
        res.redirectBack();
    }
}
