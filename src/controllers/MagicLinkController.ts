import _MagicLinkController from "wms-core/auth/magic_link/MagicLinkController";
import MagicLink from "wms-core/auth/models/MagicLink";
import {Request, Response} from "express";
import MagicLinkWebSocketListener from "wms-core/auth/magic_link/MagicLinkWebSocketListener";
import {MagicLinkActionType} from "./MagicLinkActionType";
import Controller from "wms-core/Controller";
import {BadOwnerMagicLink} from "wms-core/auth/magic_link/MagicLinkAuthController";
import UserEmail from "wms-core/auth/models/UserEmail";
import App from "../App";
import AuthComponent from "wms-core/auth/AuthComponent";

export default class MagicLinkController extends _MagicLinkController<App> {
    public constructor(magicLinkWebSocketListener: MagicLinkWebSocketListener<App>) {
        super(magicLinkWebSocketListener);
    }

    protected async performAction(magicLink: MagicLink, req: Request, res: Response): Promise<void> {
        if (magicLink.action_type === MagicLinkActionType.ADD_RECOVERY_EMAIL) {
            if (!req.session || !req.sessionID || magicLink.session_id !== req.sessionID) throw new BadOwnerMagicLink();

            await magicLink.delete();

            const authGuard = this.getApp().as(AuthComponent).getAuthGuard();
            const proof = await authGuard.isAuthenticated(req.session);
            const user = await proof?.getResource();
            if (!user) return;

            const email = await magicLink.getOrFail('email');
            if (await UserEmail.select().with('user').where('email', email).first()) {
                req.flash('error', 'An account already exists with this email address. Please first remove it there before adding it here.');
                res.redirect(Controller.route('account'));
                return;
            }

            const userEmail = UserEmail.create({
                user_id: user.id,
                email: email,
                main: false,
            });
            await userEmail.save();

            if (!user.main_email_id) {
                user.main_email_id = userEmail.id;
                await user.save();
            }

            req.flash('success', `Recovery email ${userEmail.email} successfully added.`);
            res.redirect(Controller.route('account'));
        }
    }
}
