import Application from "wms-core/Application";
import Migration, {MigrationType} from "wms-core/db/Migration";
import ExpressAppComponent from "wms-core/components/ExpressAppComponent";
import NunjucksComponent from "wms-core/components/NunjucksComponent";
import MysqlComponent from "wms-core/components/MysqlComponent";
import LogRequestsComponent from "wms-core/components/LogRequestsComponent";
import RedisComponent from "wms-core/components/RedisComponent";
import ServeStaticDirectoryComponent from "wms-core/components/ServeStaticDirectoryComponent";
import MaintenanceComponent from "wms-core/components/MaintenanceComponent";
import MailComponent from "wms-core/components/MailComponent";
import SessionComponent from "wms-core/components/SessionComponent";
import FormHelperComponent from "wms-core/components/FormHelperComponent";
import CsrfProtectionComponent from "wms-core/components/CsrfProtectionComponent";
import WebSocketServerComponent from "wms-core/components/WebSocketServerComponent";
import HomeController from "./controllers/HomeController";
import AuthController from "./controllers/AuthController";
import AuthComponent from "wms-core/auth/AuthComponent";
import AuthGuard from "wms-core/auth/AuthGuard";
import {PasswordAuthProof} from "./models/UserPasswordComponent";
import LDAPServerComponent from "./LDAPServerComponent";
import AutoUpdateComponent from "wms-core/components/AutoUpdateComponent";
import DummyMigration from "wms-core/migrations/DummyMigration";
import DropLegacyLogsTable from "wms-core/migrations/DropLegacyLogsTable";
import AccountController from "./controllers/AccountController";
import CreateMigrationsTable from "wms-core/migrations/CreateMigrationsTable";
import CreateUsersAndUserEmailsTable from "wms-core/auth/migrations/CreateUsersAndUserEmailsTable";
import AddPasswordToUsers from "./migrations/AddPasswordToUsers";
import CreateMagicLinksTable from "wms-core/auth/migrations/CreateMagicLinksTable";
import MailController from "wms-core/auth/MailController";
import MagicLinkController from "./controllers/MagicLinkController";
import MagicLinkWebSocketListener from "wms-core/auth/magic_link/MagicLinkWebSocketListener";
import BackendController from "wms-core/helpers/BackendController";
import AddApprovedFieldToUsersTable from "wms-core/auth/migrations/AddApprovedFieldToUsersTable";
import FixUserMainEmailRelation from "wms-core/auth/migrations/FixUserMainEmailRelation";
import DropNameFromUsers from "wms-core/auth/migrations/DropNameFromUsers";
import MagicLink from "wms-core/auth/models/MagicLink";
import AddNameToUsers from "./migrations/AddNameToUsers";
import CreateMailTables from "./migrations/CreateMailTables";
import MailboxBackendController from "./controllers/backend/MailboxBackendController";
import RedirectBackComponent from "wms-core/components/RedirectBackComponent";
import MailAutoConfigController from "./controllers/MailAutoConfigController";
import AccountBackendController from "./controllers/backend/AccountBackendController";
import packageJson = require('../package.json');

export default class App extends Application {
    private magicLinkWebSocketListener?: MagicLinkWebSocketListener<this>;

    public constructor(
        private readonly addr: string,
        private readonly port: number,
    ) {
        super(packageJson.version);
    }

    protected getMigrations(): MigrationType<Migration>[] {
        return [
            CreateMigrationsTable,
            DummyMigration,
            CreateUsersAndUserEmailsTable,
            AddPasswordToUsers,
            CreateMagicLinksTable,
            AddApprovedFieldToUsersTable,
            FixUserMainEmailRelation,
            DropNameFromUsers,
            AddNameToUsers,
            CreateMailTables,
            DropLegacyLogsTable,
        ];
    }

    protected async init(): Promise<void> {
        this.registerComponents();
        this.registerWebSocketListeners();
        this.registerControllers();
    }

    private registerComponents() {
        const redisComponent = new RedisComponent();
        const mysqlComponent = new MysqlComponent();

        // Base
        const expressAppComponent = new ExpressAppComponent(this.addr, this.port);
        this.use(expressAppComponent);
        this.use(new LogRequestsComponent());

        // Static files
        this.use(new ServeStaticDirectoryComponent('public'));
        this.use(new ServeStaticDirectoryComponent('node_modules/feather-icons/dist', '/icons'));

        // Dynamic views and routes
        this.use(new NunjucksComponent());
        this.use(new RedirectBackComponent());

        // Maintenance
        this.use(new MaintenanceComponent(this, () => {
            return redisComponent.canServe() && mysqlComponent.canServe();
        }));
        this.use(new AutoUpdateComponent());

        // Services
        this.use(mysqlComponent);
        this.use(new MailComponent());

        // Session
        this.use(redisComponent);
        this.use(new SessionComponent(redisComponent));

        // Utils
        this.use(new FormHelperComponent());

        // Middlewares
        this.use(new CsrfProtectionComponent());

        // Auth
        this.use(new AuthComponent(new class extends AuthGuard<PasswordAuthProof | MagicLink> {
            public async getProofForSession(session: Express.Session): Promise<PasswordAuthProof | MagicLink | null> {
                return PasswordAuthProof.getProofForSession(session) ||
                    await MagicLink.bySessionId(session.id);
            }
        }(this)));

        // WebSocket server
        this.use(new WebSocketServerComponent(this, expressAppComponent, redisComponent));

        // LDAP server
        this.use(new LDAPServerComponent());
    }

    private registerWebSocketListeners() {
        this.magicLinkWebSocketListener = new MagicLinkWebSocketListener();
        this.use(this.magicLinkWebSocketListener);
    }

    private registerControllers() {
        // Priority routes / interrupting middlewares
        this.use(new AccountController());
        this.use(new MagicLinkController(this.as(MagicLinkWebSocketListener)));
        this.use(new BackendController());
        this.use(new MailboxBackendController());
        this.use(new AccountBackendController());
        this.use(new AuthController());
        this.use(new MailAutoConfigController()); // Needs to override MailController

        // Core functionality
        this.use(new MailController());

        // Other functionnality

        // Semi-static routes
        this.use(new HomeController());
    }
}
