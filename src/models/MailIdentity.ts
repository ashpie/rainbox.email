import Model from "wms-core/db/Model";
import User from "wms-core/auth/models/User";
import MailDomain from "./MailDomain";
import {OneModelRelation} from "wms-core/db/ModelRelation";
import {EMAIL_REGEX} from "wms-core/db/Validator";

export default class MailIdentity extends Model {
    public static get table(): string {
        return 'mail_identities';
    }

    public id?: number = undefined;
    public user_id?: number = undefined;
    public mail_domain_id?: number = undefined;
    public name?: string = undefined;
    public redirects_to?: string = undefined;

    public readonly user: OneModelRelation<MailIdentity, User> = new OneModelRelation(this, User, {
        foreignKey: 'id',
        localKey: 'user_id',
    });
    public readonly domain: OneModelRelation<MailIdentity, MailDomain> = new OneModelRelation(this, MailDomain, {
        foreignKey: 'id',
        localKey: 'mail_domain_id',
    });

    protected init(): void {
        this.setValidation('user_id').defined().exists(User, 'id');
        this.setValidation('mail_domain_id').defined().exists(MailDomain, 'id');
        this.setValidation('name').defined().maxLength(64).regexp(/^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+$/)
            .unique(this, 'name', () => MailIdentity.select().where('mail_domain_id', this.mail_domain_id));
        this.setValidation('redirects_to').acceptUndefined().maxLength(254).regexp(EMAIL_REGEX);
    }

    public async toEmail(): Promise<string> {
        return this.name + '@' + (await this.domain.get())?.name;
    }
}
