import User from "wms-core/auth/models/User";
import ModelComponent from "wms-core/db/ModelComponent";

export const USERNAME_REGEXP = /^[0-9a-z_-]+$/;

export default class UserNameComponent extends ModelComponent<User> {
    public name?: string = undefined;

    public init(): void {
        this.setValidation('name').defined().between(3, 64).regexp(USERNAME_REGEXP).unique(this._model);
    }

}
