import ModelComponent from "wms-core/db/ModelComponent";
import User from "wms-core/auth/models/User";
import {ManyModelRelation, OneModelRelation} from "wms-core/db/ModelRelation";
import MailIdentity from "./MailIdentity";
import MailDomain from "./MailDomain";

export default class UserMailIdentityComponent extends ModelComponent<User> {
    public main_mail_identity_id?: number = undefined;

    public readonly mailIdentities: ManyModelRelation<User, MailIdentity> = new ManyModelRelation(this._model,
        MailIdentity, {
            localKey: 'id',
            foreignKey: 'user_id',
        });
    public readonly publicMailIdentities: ManyModelRelation<User, MailIdentity> = this.mailIdentities.clone()
        .filter(async model => !!(await model.domain.get())?.isPublic());

    public readonly mailDomains: ManyModelRelation<User, MailDomain> = new ManyModelRelation(this._model, MailDomain, {
        localKey: 'id',
        foreignKey: 'user_id',
    });

    public readonly mainMailIdentity: OneModelRelation<User, MailIdentity> = new OneModelRelation(this._model,
        MailIdentity, {
            foreignKey: 'id',
            localKey: 'main_mail_identity_id',
        });

    public getMaxPublicAddressesCount(): number {
        return 1;
    }

    public async getPublicAddressesCount(): Promise<number> {
        return (await this.publicMailIdentities.get()).length;
    }
}
