# Rainbox Email

Rainbox Email is an ISP-like mail service database manager. It also comes with some built-in authentication provider features such as an integrated LDAP server.

For now, it only supports MySQL.

Please feel free to contribute by making issues, bug reports and pull requests.

## /!\ THIS PROJECT STILL LACKS ESSENTIAL FEATURES SUCH AS: /!\

- [x] ~~Change password~~
- [ ] Password recovery (recovery emails are unused yet)
- [ ] Quota management
- [ ] Editable terms of service
- [ ] Complex permissions system
- [ ] Redirections (can be manually setup with sql queries)
- Probably many others, please make an issue so I can add them to this list

## How does it work?

When a user register, they don't immediately get a mailbox. If approval_mode is on, then they have to get approved by an administrator. Then they have to create their first email address which will be their mailbox username.

Users can only create one email address with their username from public domains (domains set as owned by "Public").

However, domains can also be owned by a specific user in which case the user owning the domain can create infinite addresses @the-domain.com.

Note that a user only has one mailbox once they create their first address and cannot have more.

## Installation instructions & recommendations

### Requirements

- mariadb (for the mysql database that will be operated)
- redis (session, cache)
- A working mail smtp server (to send emails)
- git and yarn for updates

### Instructions

- Create a user i.e. `rainbox-email` and its home directory i.e. `/home/rainbox-email`
- `su rainbox-email` (switch to the new user)
- `git clone git@gitlab.com:ArisuOngaku/rainbox.email.git ~/live` (clone Rainbox Email into a new `live` folder)
- `cd ~/live`
- `yarn install && yarn build`
- MySQL
    - Create a new empty database named as per configuration (default is `rainbox_email`)
    - Create a new mysql user and give them all permissions on the database (`GRANT ALL PRIVILEGES ON rainbox_email.* TO rainbox_email@localhost IDENTIFIED BY "secret_password";`)
    - Don't forget to add mysql user and password in `config/local.json5` (see ###Configuration)
- Configure mail (required) and redis (if needed)
    - For a private instance, please don't forget to set `approval_mode` to `true`
- Service
    - Edit [app.service](app.service) as needed, copy to `/etc/systemd/system/rainbox-email.service`
    - When ready, enable and start the service (`systemctl enable --now rainbox-email`)
- Once started, if there were no errors then the database should be created.
- To gain administrator rights, register an account and then go back to mysql and run: `UPDATE users SET is_admin=1 WHERE name="your_user_name";`

### Configuration

Create a `config/local.json5` file and override all wanted parameters. See `config/{default,prouction}.json5` and `node_modules/wms-core/config/{default,production}.json5` for defaults.

## Postfix and dovecot mysql queries

### Postfix

- `virtual_mailbox_domains` => `SELECT 1 FROM mail_domains WHERE name='%s'`
- `virtual_mailbox_maps` => `SELECT 1 FROM users u LEFT JOIN mail_identities i ON i.user_id=u.id AND u.main_mail_identity_id=i.id LEFT JOIN mail_domains d ON i.mail_domain_id=d.id WHERE CONCAT(i.name, '@', d.name)='%s'`
- `virtual_alias_maps` => `(SELECT redirects_to FROM mail_identities i LEFT JOIN mail_domains d ON i.mail_domain_id=d.id WHERE CONCAT(i.name, '@', d.name)='%s' AND redirects_to IS NOT NULL) UNION (SELECT CONCAT(i.name, '@', d.name) FROM users u LEFT JOIN mail_identities i ON i.id=u.main_mail_identity_id LEFT JOIN mail_domains d ON i.mail_domain_id=d.id WHERE (SELECT u.id FROM users u LEFT JOIN mail_identities i ON i.user_id=u.id LEFT JOIN mail_domains d ON i.mail_domain_id=d.id WHERE CONCAT(i.name, '@', d.name)='%s' AND redirects_to IS NULL LIMIT 1)=u.id AND i.id IS NOT NULL)`
- `smtpd_sender_login_maps` => `SELECT CONCAT(i.name, '@', d.name) FROM users u LEFT JOIN mail_identities i ON i.user_id=u.id LEFT JOIN mail_domains d ON i.mail_domain_id=d.id WHERE CONCAT(i.name, '@', d.name)='%s'`

### Dovecot `dovecot-sql.conf.ext`

Note that the quota is hardcoded in the `user_query` to 5GB, please change as needed.

- `user_query = SELECT CONCAT(i.name, '@', d.name) as user, CONCAT('*:bytes=', '5368709000') as quota_rule, '/storage1/mail/%d/%n' as home, 1011 as uid, 1012 as gid FROM users u LEFT JOIN mail_identities i ON i.user_id=u.id AND u.main_mail_identity_id=i.id LEFT JOIN mail_domains d ON i.mail_domain_id=d.id WHERE CONCAT(i.name, '@', d.name)='%u'`
- `password_query = SELECT password FROM users u LEFT JOIN mail_identities i ON i.user_id=u.id AND u.main_mail_identity_id=i.id LEFT JOIN mail_domains d ON i.mail_domain_id=d.id WHERE CONCAT(i.name, '@', d.name)='%u' AND redirects_to IS NULL`
- `iterate_query = SELECT CONCAT(i.name, '@', d.name) as user FROM users u LEFT JOIN mail_identities i ON i.id=u.main_mail_identity_id LEFT JOIN mail_domains d ON i.mail_domain_id=d.id WHERE i.id IS NOT NULL`

It is recommended to set `default_pass_scheme = ARGON2ID`.
